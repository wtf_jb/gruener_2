<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Grüner
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<?php wp_nav_menu( array('menu' => 'Footer' )); ?>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>



<!--
<script>
  // Picture element HTML5 shiv
  document.createElement( "picture" );
</script>
<script src="<?php bloginfo( 'template_url' ); ?>/js/picturefill.min.js" async></script>-->


<script>
/* Hide Logo in mobile fullscreen */
var isMobile = false; //initiate as false
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

		$(document).ready(function() {
				if(isMobile){
					if ($("body").hasClass("kontakt") || $("body").hasClass("impressum")) {
						$('.logo').css('display', 'none')
					}
					if ($("body").hasClass("design-moebel-outlet")) {
						$('.logo').click(function () {
  						$('.entry-content').toggleClass( "overflow-hidden" );
						});
					}
				}
		});


</script>

<script>
	$(document).on('animated.slides', function(slide) {
		 slideIndex = $('#slides').superslides('current');
	});

	$(document).on('animated.slides', function(slide) {
		 slideIndex = $('#event-slides').superslides('current');
	});

    numberOfSlides = $('#slideshow .slides-container > li, #banner .slides-container > li').length;
    if(numberOfSlides > 1){
        playThis = 8000;
    } else{
        playThis = 0;
    }
</script>

<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/jquery.animate-enhanced.min.js"></script>

<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/jquery.superslides.js"></script>
<script>
  $(function() {
    $('#slides').superslides({
      hashchange: true
    });
  });

  $(function() {
    $('#event-slides').superslides({
      hashchange: true,
			//play: 4000
    });
  });
</script>

<script>
	$(document).on('animated.slides', function(slide) {
		 slideIndex = $('#slides').superslides('current');
		 $(".current_image").empty();
		 $(".current_image").append(slideIndex+1);
	});

	$(document).on('animated.slides', function(slide) {
		 slideIndex = $('#event-slides').superslides('current');
		 $(".current_image").empty();
		 $(".current_image").append(slideIndex+1);
	});
</script>

<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/jquery.touchSwipe.js"></script>
<script>
	$(function() {
		//Enable swiping on Landingpage
		$(".has_slider").swipe( {
				//Generic swipe handler for all directions
					swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
						$(this).superslides('animate', 'next');
					},
					swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
						$(this).superslides('animate', 'prev');
					},
				//Default is 75px, set to 0 for demo so any distance triggers swipe
				threshold:75
		});
	});

</script>

<script>
	$("body.2015-lieblingsstuecke").keydown(function(e) {
		if(e.keyCode == 37) { // left
    	$(".tooltip-content-fullscreen").css("display", "none");
	    $(".tooltip-content").css("display", "none");
			$('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/plus.svg)");
		}
		if(e.keyCode == 39) { // right
    	$(".tooltip-content-fullscreen").css("display", "none");
	    $(".tooltip-content").css("display", "none");
			$('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/plus.svg)");
		}
	});
	$("body.2015-lieblingsstuecke .next").click(function(){
		$(".tooltip-content-fullscreen").css("display", "none");
		$(".tooltip-content").css("display", "none");
		$('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/plus.svg)");
	});
	$("body.2015-lieblingsstuecke .prev").click(function(){
		$(".tooltip-content-fullscreen").css("display", "none");
		$(".tooltip-content").css("display", "none");
		$('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/plus.svg)");
	});
</script>
<script>
	$(".tooltip-clickable").click(function(){
	    $(".tooltip-content").fadeToggle("fast", function(){});
			$(".tooltip-content-fullscreen").fadeToggle("fast", function(){});
			if($(".tooltip-content-fullscreen").is(':visible')){
				if($(".tooltip-content-fullscreen").hasClass('visible')){
					$('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/plus.svg)");
					$(".tooltip-content-fullscreen").removeClass('visible');
					if(isMobile){
						$('.logo').css('display', 'block')
					}
				} else {
					$('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/kreuz.svg)");
					$(".tooltip-content-fullscreen").addClass('visible');
					if(isMobile){
						$('.logo').css('display', 'none')
					}
				}
			}
	});
</script>

<!-- Event Popup always visible 
<script>

	$(document).ready(function() {
        if ($("body").hasClass("single-events")) {
            $(".tooltip-content-fullscreen").fadeToggle("fast", function(){});
            $('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/kreuz.svg)");
            $(".tooltip-content-fullscreen").addClass('visible');
            if(isMobile){
                $('.logo').css('display', 'none')
            }
        }
	});

</script>
-->


<script>
	$(".tooltip-content-fullscreen").click(function(){
			$(".tooltip-content-fullscreen").fadeToggle("fast", function(){});
			if($(".tooltip-content-fullscreen").is(':visible')){
				if($(".tooltip-content-fullscreen").hasClass('visible')){
					$('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/plus.svg)");
					if(isMobile){
						$('.logo').css('display', 'block')
					}
					$(".tooltip-content-fullscreen").removeClass('visible');
				} else {
					$('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/kreuz.svg)");
					if(isMobile){
						$('.logo').css('display', 'none')
					}
					$(".tooltip-content-fullscreen").addClass('visible');
				}
			}
	});
</script>
<script>
	$(".impressum .tooltip-clickable").click(function(){
			if($(".tooltip-content-fullscreen").is(':visible')){
				if($(".tooltip-content-fullscreen").hasClass('visible')){
					$('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/plus.svg)");
					if(isMobile){
						$('.logo').css('display', 'block')
					}
				}else {
						$('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/kreuz.svg)");
						if(isMobile){
							$('.logo').css('display', 'none')
						}
				}
			} else {
					$('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/kreuz.svg)");
					if(isMobile){
						$('.logo').css('display', 'none')
					}
			}
	});
	$(".impressum .tooltip-content-fullscreen").click(function(){
			$(".tooltip-content-fullscreen").fadeToggle("fast", function(){});
			if($(".tooltip-content-fullscreen").is(':visible')){
				if($(".tooltip-content-fullscreen").hasClass('visible')){
					$('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/plus.svg)");
					$(".tooltip-content-fullscreen").removeClass('visible');
					if(isMobile){
						$('.logo').css('display', 'block')
					}
				}
			} else {
					$('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/kreuz.svg)");
					$(".tooltip-content-fullscreen").addClass('visible');
					if(isMobile){
						$('.logo').css('display', 'none')
					}
			}
	});
	$(".kontakt .tooltip-clickable").click(function(){
			if($(".tooltip-content-fullscreen").is(':visible')){
				if($(".tooltip-content-fullscreen").hasClass('visible')){
					$('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/plus.svg)");
					if(isMobile){
						$('.logo').css('display', 'block')
					}
				}else {
						$('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/kreuz.svg)");
						if(isMobile){
							$('.logo').css('display', 'none')
						}
				}
			} else {
					$('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/kreuz.svg)");
					if(isMobile){
						$('.logo').css('display', 'none')
					}
			}
	});
	$(".kontakt .tooltip-content-fullscreen").click(function(){
			$(".tooltip-content-fullscreen").fadeToggle("fast", function(){});
			if($(".tooltip-content-fullscreen").is(':visible')){
				if($(".tooltip-content-fullscreen").hasClass('visible')){
					$('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/plus.svg)");
					$(".tooltip-content-fullscreen").removeClass('visible');
					if(isMobile){
						$('.logo').css('display', 'block')
					}
				}
			} else {
					$('.tooltip').css("background-image", "url(<?php bloginfo( 'template_url' ); ?>/img/kreuz.svg)");
					$(".tooltip-content-fullscreen").addClass('visible');
					if(isMobile){
						$('.logo').css('display', 'none')
					}
			}
	});
</script>

<script>
	$( ".logo" ).click(function() {
	  $( ".navigation" ).fadeToggle( "fast", "linear" );
		if ($(".black")[0]){
			$(".logo").attr('src',"<?php bloginfo( 'template_url' ); ?>/img/logo_white.png");
			$(".logo").removeClass("black");
		} else {
			$(".logo").attr('src',"<?php bloginfo( 'template_url' ); ?>/img/logo_black.png");
			$(".logo").addClass("black");
		}
	});
</script>
<script>
	$( ".navigation" ).click(function() {
	  $( ".navigation" ).fadeToggle( "fast", "linear" );
		if ($(".black")[0]){
			$(".logo").attr('src',"<?php bloginfo( 'template_url' ); ?>/img/logo_white.png");
			$(".logo").removeClass("black");
		} else {
			$(".logo").attr('src',"<?php bloginfo( 'template_url' ); ?>/img/logo_black.png");
			$(".logo").addClass("black");
		}
	});
</script>

<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/jquery.cookie.min.js"></script>
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/jquery.navgoco.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#navigation-list").navgoco({accordion: true});
	});
</script>
<!--
<script>
	$(document).ready(function() {
		if(localStorage.getItem('navigation_hint') != 'shown'){ //only show hint if it wasn't shown before
			$(".navigation_hint").delay(10000).fadeIn("slow"); //show hint after 10 seconds
			localStorage.setItem('navigation_hint','shown') //localStorage that hint was shown
		}

		$(document).click(function() {
			$(".navigation_hint").fadeOut("slow"); //fade out hint if click anywhere in page
		});
	});


</script>
-->
<script>
	$(document).ready(function() {
	$('body.occhio .content-area .slides-container').click(function(){
			window.open('https://de.occhio.de/online-shop?pid=1711a4&utm_source=partner&utm_campaign=Kampagne&utm_medium=1711a4-Grüner+GmbH&utm_term=1711a4&utm_content=myOcchio_Occhio_Partner_Campaign_full2880.jpg');
		});
		$("body.occhio .content-area .slides-container").css('cursor', 'pointer');
	});


</script>
<!--

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>


<div id="start-popup" class="modal" style="display: none;">
  <a href="gewinnspiel"><img src="https://gruener-gerstetten.de/wp-content/uploads/neuigkeiten_gruener_my_occhio_popup.jpg" /></a>
</div>


<script>
	$(document).ready(function() {
    if ($('body.occhio').length > 0)
    {
$('#start-popup').modal();
    }

	});


</script>
-->
</body>
</html>
