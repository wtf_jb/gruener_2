<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Grüner
 */

?>

<?php if(is_singular( 'hersteller' )) {
	?>
	<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
			<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
		<ul class="slides-container">

			<?php	$bg_image = get_field('bg-image');
			if( $bg_image ): ?>

			<img src="<?php echo $bg_image; ?>">

			<?php endif; ?>

		</ul>

		<nav class="slides-navigation">
			<a href="#" class="next">Next</a>
			<a href="#" class="prev">Previous</a>
		</nav>
	</div>

	<?php $pagination_images = get_field('bg-slider');
	$ct_pg_img = count($pagination_images);
	if ($ct_pg_img > 1) {?>
	<div class="new-slides-pagination">
		<span class="current_image"></span>
		<span class="seperator">&#124;</span>
		<?php echo $ct_pg_img; ?>
	</div>
	<?php } ?>

	<div class="tooltip-wrapper">
		<div class="tooltip">
			<div class="tooltip-content">
				<div class="title"><?php if(get_field('headline')){the_field('headline');}else{the_title();}; ?></div></ br>
				<div class="desc"><?php the_field('description'); ?></div>
				<div class="url"><a href="http://<?php the_field('hersteller_url'); ?>" target="_blank"><?php the_field('hersteller_url'); ?></a></div>
			</div>
			<div class="tooltip-clickable"></div>
		</div>
	</div>

<?php } elseif(is_singular( 'events' )) {
	if (get_field('special_event')){ $is_se = "special-event"; }

	?>
	<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
	<div id="event-slides" class="bg-slider events <?php echo $is_se; ?> <?php echo $slider_class; ?>">
		<ul class="slides-container">

			<?php
			$image_count = 0;
			$images = get_field('bg-slider');
			if( $images ):
				foreach( $images as $image ):
					$image_count++;?>

					<li class="<?php echo "slide_nr_" . $image_count; ?>">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">

					<?php
					if (get_field('special_event')) {


						if ($image_count == 1) { ?>
							<div class="tooltip-content-fullscreen">
								<div class="desc"><span class="title"><?php the_field('headline'); ?></span></ br>
								<?php the_field('description'); ?>
								</div>
							</div>
						<?php } ?>
							<div class="tooltip-wrapper">
								<div class="tooltip">
									<?php if ($image_count > 1) { ?>
										<div class="tooltip-content">
											<div class="title"><?php echo $image['title']; ?></div></ br>
												<div class="desc"><?php echo $image['description']; ?></div></ br></ br>
												<div class="desc"><?php echo $image['caption']; ?></div>
										</div>
									<?php } ?>
									<div class="tooltip-clickable"></div>
								</div>
							</div>
					<?php } else {?>



					<div class="tooltip-wrapper">
						<div class="tooltip">
							<div class="tooltip-clickable"></div>
						</div>
					</div>
					<div class="tooltip-content-fullscreen">
						<div class="desc"><span class="title"><?php the_field('headline'); ?></span></ br>
						<?php the_field('description'); ?>
						</div>
					</div>

					<?php } ?>
				</li>
				<?php
				endforeach;
		 	endif; ?>
		</ul>

		<nav class="slides-navigation">
			<a href="#" class="next">Next</a>
			<a href="#" class="prev">Previous</a>
		</nav>
	</div>

	<?php $pagination_images = get_field('bg-slider');
	$ct_pg_img = count($pagination_images);
	if ($ct_pg_img > 1) {?>
	<div class="new-slides-pagination">
		<span class="current_image"></span>
		<span class="seperator">&#124;</span>
		<?php echo $ct_pg_img; ?>
	</div>
	<?php } ?>

	<?php } else { ?>

	<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
		<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
			<ul class="slides-container">

				<?php
				// name of gallery-field
				$images = get_field('bg-slider');
				// check if image is not empty
				if( $images ):
				 	// loop through the images
					foreach( $images as $image ):
				?>

				<li>
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
				</li>

				<?php
				endforeach;
				else :
				    // if no rows found
				endif;
				?>

			</ul>

			<nav class="slides-navigation">
				<a href="#" class="next">Next</a>
				<a href="#" class="prev">Previous</a>
			</nav>

	</div>

		<div class="tooltip-wrapper">
			<div class="tooltip">
				<?php if (!get_field('fullscreen_content') ) : ?>
					<div class="tooltip-content">
						<div class="title"><?php if(get_field('headline')){the_field('headline');}else{the_title();}; ?></div></ br>
						<div class="desc"><?php the_field('description'); ?></div>
					</div>
				<?php endif; ?>
				<div class="tooltip-clickable"></div>
			</div>
		</div>

		<?php $pagination_images = get_field('bg-slider');
		$ct_pg_img = count($pagination_images);
		if ($ct_pg_img > 1) {?>
		<div class="new-slides-pagination">
			<span class="current_image"></span>
			<span class="seperator">&#124;</span>
			<?php echo $ct_pg_img; ?>
		</div>
		<?php } ?>


<?php } ?>

<?php if (get_field('fullscreen_content') && !is_singular( 'events' )) : ?>
<div class="tooltip-content-fullscreen">
	<div class="desc"><span class="title"><?php the_field('headline'); ?></span></ br>
	<?php the_field('description'); ?>
	</div>
</div>
<?php endif; ?>
