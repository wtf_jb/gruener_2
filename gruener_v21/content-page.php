<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Grüner
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if(is_page( 'design-moebel-outlet' )){ ?>

			<div class="entry-content">
					<div class="dmo-box dmo<?php the_field('feld_1'); ?>">
							<div class="content">
								<?php getRssfeed("http://design-moebeloutlet.de/?cat=". get_field('feld_1') ."&feed=rss2","web-spirit","auto",1,1); ?>
							</div>
					</div>
					<div class="dmo-box dmo<?php the_field('feld_2'); ?>">
							<div class="content">
								<?php getRssfeed("http://design-moebeloutlet.de/?cat=". get_field('feld_2') ."&feed=rss2","web-spirit","auto",1,1); ?>
							</div>
					</div>
					<div class="dmo-box dmo-logo">
					</div>
					<div class="dmo-box dmo<?php the_field('feld_3'); ?>">
							<div class="content">
								<?php getRssfeed("http://design-moebeloutlet.de/?cat=". get_field('feld_3') ."&feed=rss2","web-spirit","auto",1,1); ?>
							</div>
					</div>
			</div>

			<?php } elseif(is_page( 'startseite' )) { ?>

				<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
						<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
								<ul class="slides-container">

									<?php

									if(get_field('latest_event')) {

										$event_filter = get_field('event_filter');

										$post = $event_filter;

										setup_postdata( $post );

											$images = get_field('bg-slider');

											$image_id = $images[0];

											if( $image_id ):
											?>
												<li>
													<a href="<?php the_permalink(); ?>/#2"><img src="<?php echo $image_id['url']; ?>" alt="<?php echo $image_id['alt']; ?>"></a>

													<div class="tooltip-wrapper">
														<div class="tooltip">
															<div class="tooltip-clickable"></div>
														</div>
													</div>
													<div class="tooltip-content-fullscreen">
														<div class="desc">
															<div class="desc_relative">
																<span class="title"><?php the_field('headline'); ?></span></ br>
														<?php the_field('description'); ?>
													</div>
														</div>
													</div>

												</li>

											<?php

										wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
										endif;

									} else {


										$rows = get_field('referenzen_filter');
										if($rows)
										{
											shuffle( $rows );

											$row = $rows[0];
											$hersteller_nr = $row['referenz_nr'];
											$post = $hersteller_nr;
											setup_postdata( $post );

												$bg_images = get_field('bg-slider');
												$bg_image = $bg_images[0];

												?>
													<li>
														<a href="<?php the_permalink(); ?>"><img src="<?php echo $bg_image[url]; ?>" ></a>
													</li>

												<?php

											wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
										}
									}
									?>

								</ul>

								<nav class="slides-navigation">
									<a href="#" class="next">Next</a>
									<a href="#" class="prev">Previous</a>
								</nav>
							</div>

			<?php } elseif(is_page( 'impressum' )) { ?>

				<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
						<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
								<ul class="slides-container impressum">
									<?php
									// name of gallery-field
									$images = get_field('bg-slider');
									// check if image is not empty
									if( $images ):
										// loop through the images
										foreach( $images as $image ):
									?>
												<li>
													<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
													<div class="tooltip-wrapper">
														<div class="tooltip">
															<div class="tooltip-clickable"></div>
														</div>
													</div>
													<div class="tooltip-content-fullscreen">
														<div class="desc">
															<div class="desc_relative">
																<span class="title"><?php the_field('headline'); ?></span></ br>
														<?php the_field('description'); ?>
													</div>
														</div>
													</div>

												</li>


												<?php
												endforeach;
												endif;
												?>
											</ul>

											<nav class="slides-navigation">
												<a href="#" class="next">Next</a>
												<a href="#" class="prev">Previous</a>
											</nav>
										</div>


			<?php } elseif(is_page('kontakt')) {
				$kontaktbild = get_field('kontaktbild');
				?>
				<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
						<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
								<ul class="slides-container kontakt">
									<?php
									// name of gallery-field
									$images = get_field('bg-slider');
									// check if image is not empty
									if( $images ):
										// loop through the images
										foreach( $images as $image ):
									?>
												<li>
													<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
													<div class="tooltip-wrapper">
														<div class="tooltip">
															<div class="tooltip-clickable"></div>
														</div>
													</div>
													<div class="tooltip-content-fullscreen">
														<div class="desc">
															<div class="desc_relative">
																<div class="title"><?php the_field('headline'); ?></div></ br>
																<div class="kontaktdaten"><?php the_field('description'); ?></div></ br>
																<div class="ansprechpartner">
																	<img src="<?php echo $kontaktbild['url']; ?>" alt="<?php echo $kontaktbild['alt']; ?>" class="kontaktbild"></ br>
																	<div class="ansprechpartner_content"><?php the_field('description2'); ?></div></ br>
																</div>
															<div class="oeffnungszeiten"><?php the_field('description3'); ?></div></ br>
													</div>
														</div>
													</div>

												</li>


												<?php
												endforeach;
												endif;
												?>
											</ul>

											<nav class="slides-navigation">
												<a href="#" class="next">Next</a>
												<a href="#" class="prev">Previous</a>
											</nav>
										</div>


			<?php } else { ?>

<?php if (count(get_field('bg-slider')) > 1) { $slider_class = "has_slider";} ?>
		<div id="slides" class="bg-slider referenzen <?php echo $slider_class; ?>">
			<ul class="slides-container">
				<?php $images = get_field('bg-slider');
				if( $images ): foreach( $images as $image ): ?>

					<li><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></li>

				<?php endforeach; endif; ?>
			</ul>

			<nav class="slides-navigation"><a href="#" class="next">Next</a><a href="#" class="prev">Previous</a></nav>
		</div>

		<?php $pagination_images = get_field('bg-slider');
		$ct_pg_img = count($pagination_images);
		if ($ct_pg_img > 1) {?>
		<div class="new-slides-pagination"><span class="current_image"></span><span class="seperator">&#124;</span><?php echo $ct_pg_img; ?></div>
		<?php } ?>

			<div class="tooltip-wrapper">
				<div class="tooltip">
					<?php if (!get_field('fullscreen_content') ) : ?>
						<div class="tooltip-content">
							<div class="title"><?php if(get_field('headline')){the_field('headline');}else{the_title();}; ?></div></ br>
							<div class="desc"><?php the_field('description'); ?></div>
						</div>
					<?php endif; ?>
					<div class="tooltip-clickable"></div>
				</div>
			</div>


	<?php } ?>

</article><!-- #post-## -->



<?php if (get_field('fullscreen_content') ) : ?>
<div class="tooltip-content-fullscreen">
	<div class="desc"><span class="title"><?php the_field('headline'); ?></span></ br>
	<?php the_field('description'); ?></div>
	</div>
</div>
<?php endif; ?>
