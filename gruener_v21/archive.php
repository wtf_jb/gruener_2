<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Grüner
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<div id="slides" class="bg-slider events has_slider">
			<ul class="slides-container">

			<?php
			$hersteller_count = 0;
			query_posts($query_string . '&orderby=title&order=ASC');
			while ( have_posts() ) : the_post(); ?>

							<li class="<?php echo "slide_nr_" . $image_count; ?>">

								<img src="<?php the_field('bg-image') ?>">

								<div class="tooltip-wrapper">
									<div class="tooltip">
										<div class="tooltip-content">
											<div class="title"><?php if(get_field('headline')){the_field('headline');}else{the_title();}; ?></div></ br>
											<div class="desc"><?php the_field('description'); ?></div>
											<div class="url"><a href="http://<?php the_field('hersteller_url'); ?>" target="_blank"><?php the_field('hersteller_url'); ?></a></div>
										</div>
										<div class="tooltip-clickable"></div>
									</div>
								</div>

							</li>

			<?php
			$hersteller_count++;
			endwhile; // End of the loop. ?>

	</ul>

	<nav class="slides-navigation">
		<a href="#" class="next">Next</a>
		<a href="#" class="prev">Previous</a>
	</nav>
	</div>

	<?php
	if ($hersteller_count > 1) {?>
	<div class="new-slides-pagination">
		<span class="current_image"></span>
		<span class="seperator">&#124;</span>
		<?php echo $hersteller_count; ?>
	</div>
	<?php } ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
