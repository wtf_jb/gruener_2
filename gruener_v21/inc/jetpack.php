<?php
/**
 * Jetpack Compatibility File.
 *
 * @link https://jetpack.me/
 *
 * @package Grüner
 */

/**
 * Add theme support for Infinite Scroll.
 * See: https://jetpack.me/support/infinite-scroll/
 */
function gruener_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'render'    => 'gruener_infinite_scroll_render',
		'footer'    => 'page',
	) );
} // end function gruener_jetpack_setup
add_action( 'after_setup_theme', 'gruener_jetpack_setup' );

/**
 * Custom render function for Infinite Scroll.
 */
function gruener_infinite_scroll_render() {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'template-parts/content', get_post_format() );
	}
} // end function gruener_infinite_scroll_render
